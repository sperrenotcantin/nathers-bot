# The image is built on top of one that has node preinstalled
FROM node:14.7.0
# Create app directory
WORKDIR .
# Copy all files into the container
COPY . .
# Install dependencies
RUN npm install
# Open appropriate port 
EXPOSE 3000
# Start the application
CMD [ "node", "main.js" ]
