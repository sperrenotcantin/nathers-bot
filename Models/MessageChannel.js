const mongoose = require("mongoose");

const guildSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    message_id: {
        "type": String
    },
    channel_id: {
        "type": String
    }
});

module.exports = mongoose.model("MessageChannel", guildSchema);