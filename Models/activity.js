const mongoose = require("mongoose");
const { DEFAULTSETTING: defaults } = require("../config");

const guildSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: {
        "type": Date,
        "default": Date.now()
    },
    name: {
        "type": String,
        "default": ""
    },
    roles: {
        "type": Array,
        "default": []
    },
});

module.exports = mongoose.model("Activity", guildSchema);