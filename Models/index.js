module.exports = {
    "Guild": require("./guild"),
    "Poll": require("./poll"),
    "Activity": require("./activity"),
    "MessageChannel": require("./MessageChannel"),
}
