const mongoose = require("mongoose");
const { DEFAULTSETTING: defaults } = require("../config");

const guildSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    proposal: {
        "type": Array,
        "default": []
    },
    proposal_result: {
        "type": Array,
        "default": []
    },
});

module.exports = mongoose.model("Poll", guildSchema);