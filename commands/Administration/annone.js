const { MessageEmbed } = require("discord.js");
module.exports.run = async (client, message, args) => {

    const config = client.config.ANNOUNCE

    client.channels.resolve(config.channel).send(new MessageEmbed({
        title: "Nouvelle annonce !",
        description: args.join(" "),
        color: client.config.COLOR,
        timestamp: Date.now(),
        thumbnail: message.attachments.array()[0] ? message.attachments.array()[0].proxyURL : "",
        author: {
            name: message.author.username,
            iconURL: message.author.avatarURL()
        }
    }))

    for (const role of message.mentions.roles.array()) {
        await client.channels.resolve(config.channel).send("<@&"+ role.id +">")
        await client.channels.resolve(config.channel).bulkDelete(1)
    }

}

module.exports.help = {
    name: 'announce',
    description: "Annonce",
    category: "Administration",
    aliases: ["annonce", "an", "announce"],
    args: false,
    permissions: true,
    permission: ["ADMINISTRATOR"]
}