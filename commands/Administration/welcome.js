const { MessageEmbed } = require("discord.js");

module.exports.run = (client, message, args) => {
    client.welcome_channel.set(message.guild.id, message.channel.id)
}

module.exports.help = {
    name: 'welcome',
    description: "Définit le channel de bienvenue",
    category: "administration",
    args: false,
    aliases: ["welcome"],
    permissions: true,
    permission: ["ADMINISTRATOR"]
}
