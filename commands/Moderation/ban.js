module.exports.run = (client, message, args) => {
    const user = message.mentions.users.first();
    const reason = args.splice(1).join(" ");
    user ? message.guild.member(user).ban(reason) : message.channel.send("L'utilisateur n'existe pas !")
}

module.exports.help = {
    name: 'ban',
    description: "Ban l'utilisateur mentionné",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["ban", "bannir"],
    usage: "<user> <reason>",
    permissions: true,
    permission: ["BAN_MEMBERS"]
}
