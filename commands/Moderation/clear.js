const client = require("../../main")

module.exports.run = (client, message, args) => {
    if (!args.length) {
        message.delete()
        message.channel.bulkDelete(client.config.clearChat)
    } else if (args.length > 1) {
        message.channel.send(`il nous faut qu'un argument pour cette command, ${message.author} \nVoici comment utiliser la commande: \`${client.config.prefix}${command.help.name} ${command.help.usage}\``)
    } else {
        message.delete()
        message.channel.bulkDelete(args[0])
    }
}

module.exports.help = {
    name: 'clear',
    description: `Clear le chat`,
    category: "moderation",
    args: false,
    cooldown: 10,
    aliases: ["clearchat", "purge", "clear"],
    usage: "<nb_message>",
    permissions: true,
    permission: ["MANAGE_MESSAGES"]
}
