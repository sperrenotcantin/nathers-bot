module.exports.run = (client, message, args) => {
    const user = message.mentions.users.first();
    const reason = args.splice(1).join(" ");
    user ? message.guild.member(user).kick(reason) : message.channel.send("L'utilisateur n'existe pas !")
}

module.exports.help = {
    name: 'kick',
    description: "kick du server l'utilisateur mentionné",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["rep", "say"],
    usage: "<user> <reason>",
    permissions: true,
    permission: ["KICK_MEMBERS"]
}
