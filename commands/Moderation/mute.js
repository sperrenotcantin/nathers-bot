const ms = require("ms")

module.exports.run =  async (client, message, args) => {
    const member = message.guild.member(message.mentions.users.first());
    let muterole = message.guild.roles.cache.find(r => r.name === 'muted')
    let muteTime = (args[1] || '60s')

    if (!muterole) {
        muterole = await message.guild.roles.create({
            data: {
                name: "muted",
                color: "#000",
                permissions: []
            }
        });
        message.guild.channels.cache.forEach(async (channel, id) => {
            await channel.updateOverwrite(muterole, {
                SEND_MESSAGES: false,
                ADD_REACTIONS: false,
                CONNECT: false,
            });
        });
    }

    await member.roles.add(muterole.id)
    message.channel.send(`<@${member.id}> est muté pour ${ms(ms(muteTime))}.`)
    setTimeout(() => {
        if (member.roles.cache.find(r => r.name === 'muted')) {
            member.roles.remove(muterole.id)
            member.send("Vous venez d'être unmute !")
        }
    }, ms(muteTime))
}

module.exports.help = {
    name: 'mute',
    description: "mute dans les channel vocal et textuel l'utilisateur mentionné",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["mute"],
    usage: "<user> <reason>",
    permissions: true,
    permission: ["MANAGE_MESSAGES", "MUTE_MEMBERS"]
}
