const {  } = require("../../config")

module.exports.run = async (client, message, args) => {
    let user = message.guild.member(message.mentions.users.first());
    if (isNaN(args[1]) || (args[1] < 1 || args[1] > 100)) return message.reply('Il faut spécifié un ***nombre*** entre 1 et 100!')

    const messages = (await message.channel.messages.fetch({
        limit: 100,
        before: message.id,
    })).filter(a => a.author.id === user.id).array();

    messages.length = Math.min(args[1], 100)

    if (messages.length === 0 || !user) return message.reply("Aucun message à supprimer sur cet utilisateur (ou l'utilisateur n'existe pas).")

    if (messages.length === 1) await messages[0].delete();
    else await message.channel.bulkDelete(messages);

}

module.exports.help = {
    name: 'prune',
    description: `Clear le chat de l'utilisateur spécifié`,
    category: "moderation",
    args: false,
    cooldown: 10,
    aliases: ["prune"],
    usage: "<user> <nb_message>",
    permissions: true,
    permission: ["MANAGE_MESSAGES"]
}
