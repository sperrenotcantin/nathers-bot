const ms = require("ms")

module.exports.run =  (client, message, args) => {
    const member = message.guild.member(message.mentions.users.first());
    let banTime = (args[1] || '1h')
    const reason = args.splice(2).join(" ");
    member ? message.guild.member(member).ban(reason) : message.channel.send("L'utilisateur n'existe pas !")

    setTimeout(() => {
        if (!member) {message.channel.send("L'utilisateur n'existe pas !")}
        message.guild.members.unban(member)
    }, ms(banTime))
}

module.exports.help = {
    name: 'tempban',
    description: "Ban temporairement un utilisateur mentionné",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["tban", "tempban"],
    usage: "<user> <reason> <time_second>",
    permissions: true,
    permission: ["BAN_MEMBERS"]
}
