module.exports.run = async (client, message, args) => {
    let user = await client.users.fetch(args[0]);

    if (!user) {message.channel.send("L'utilisateur n'existe pas !")}
    await message.guild.members.unban(user)
}

module.exports.help = {
    name: 'unban',
    description: "Unban un utilisateur bannie",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["prune", "unban"],
    usage: "<user_id>",
    permissions: true,
    permission: ["BAN_MEMBERS"]
}
