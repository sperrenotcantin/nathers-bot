module.exports.run = (client, message, args) => {
    const member = message.guild.member(message.mentions.users.first());
    let muterole = message.guild.roles.cache.find(r => r.name === 'muted')
    member.roles.remove(muterole.id)
}

module.exports.help = {
    name: 'unmute',
    description: "Unmute un utilisateur mentionné",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["unmute"],
    usage: "<user>",
    permissions: true,
    permission: ["MANAGE_MESSAGES", "MUTE_MEMBERS"]
}
