const {MessageEmbed} = require("discord.js")
module.exports.run = async (client, message, args) => {
    if (args.length % 2 === 1) {
        return message.channel.send("Mauvaise utilisation")
    }

    let numberOfProposal = args.length / 2
    const poll_channel = client.channels.resolve(client.config.POLL.channel_poll_id)
    const react = []
    let proposal = []

    let embed = new MessageEmbed({
        description: "Pour voter réagissez avec les emojis proposés !",
        color: client.config.COLOR
    })

    let proposal_display = (message, i) => {
        console.log(message.channel.messages.resolveID(args[i * 2]))
        return args[i * 2]
    }

    for (let i = 0; i < numberOfProposal; i++) {
        proposal.push(args[i * 2])
        react.push(args[(i * 2) + 1])
        embed.addField("Proposition " + (i + 1) + ": " + react[i], proposal_display(message, i), true)
    }

    embed.setTitle('Sondage !')
    let mess = await poll_channel.send(embed)
    await poll_channel.send("<@&816033394647236678>")
    await poll_channel.bulkDelete(1)

    for (let i = 0; i < numberOfProposal; i++) {
        mess.react(react[i])
    }

    setTimeout(async () => {
        let proposal_result = []

        mess.reactions.cache.array().forEach(element => proposal_result.push(element.count - 1))
        await client.createPoll({proposal: proposal, proposal_result: proposal_result})
        let index = proposal_result.indexOf(Math.max(...proposal_result))

        let newEmbed = new MessageEmbed({
            title: "Résultat Sondages",
            description: "La proposition qui a le plus de voix est `" + proposal[index] + "` avec " + proposal_result[index] + " vote(s)",
            color: client.config.COLOR
        })

        const annonce_channel = client.channels.resolve(client.config.POLL.channel_annonce_id)
        annonce_channel.send(newEmbed)
    }, 2 * 60 * 60 * 1000)
}

module.exports.help = {
    name: 'sondage',
    description: "Annonce un sondage dans le channel approprié et fait l'annonces du résultat 24h plus tard",
    category: "misc",
    args: true,
    cooldown: 10,
    aliases: ["sd", "sondage", "poll"],
    usage: "<first element> <second element> ..",
    permissions: true,
    permission: ["ADMINISTRATOR"]
}