const {MessageEmbed} = require("discord.js");

module.exports.run = (client, message, args) => {
    let category = message.guild.channels.resolve(client.config.TEMP_CHANNEL.category_id);
    if (!category) throw new Error("Category channel does not exist");
    message.guild.channels.create(args[0], {
        type: args[1] ? args[1] : "text",
        parent: category.id,
    }).then(channel => {
        message.channel.bulkDelete(1)
        channel.overwritePermissions([
            {
                id: "715646194625282149",
                deny: ['VIEW_CHANNEL'],
            },
        ], 'Needed to change permissions');
        const embed = new MessageEmbed({
            description: "pour avoir acces au channel <#"+channel.id+"> réagisser à ce message",
            color: client.config.COLOR
        })
        message.channel.send(embed).then(new_message => {
            client.database.create(client.models.MessageChannel, {message_id: new_message.id, channel_id: channel.id}).then(r => console.log("New MessageChannel" + r.toString() ))
        })

    }).catch(console.error);

}

module.exports.help = {
    name: 'channel',
    description: "permet de gérer les channels temporaires",
    category: "membre",
    args: true,
    aliases: ["chan", "channel"],
    permissions: true,
    permission: ["MOVE_MEMBERS"]
}
