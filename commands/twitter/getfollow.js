const axios = require('axios');

// Make a request for a user with a given ID

module.exports.run = (client, message, args) => {
    client.twitter.get("followers/ids", {cursor:-1, screen_name: args[0]}).then(element => {
        message.channel.send(element.ids.length)
    })
}

module.exports.help = {
    name: 'getfollow',
    description: "récupérer les follower d'un utilisateur",
    category: "moderation",
    args: true,
    cooldown: 10,
    aliases: ["getfollow", "gf"],
    usage: "<user_name or user_id>",
    permissions: false,
}
