module.exports = (client) => {
    const config = client.config.TEMP_CHANNEL
    const guild = client.guilds.resolve(client.config.GUILD)
    const category = guild.channels.resolve(config.category_id)
    category.children.each(element => {
        console.log(element)
        if (element.type === "text") {
            const lastMessage = element.messages.resolve()
            console.log("==========================================")
            console.log(lastMessage)
            const dt = lastMessage.createdAt
            if (dt.setMinutes(dt.getMinutes() + (config.Milliseconds_check / 60 / 30)) >= Date.now()) {
                return element.delete()
            }
        } else if (element.type === "voice" && element.members.array().length <= 0) {
            return element.delete()
        }
    })
}