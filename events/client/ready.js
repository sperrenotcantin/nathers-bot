module.exports = async (client) => {
    client.emit("twitter", client)
    client.emit("reactionMessage", client)
    client.emit("memberCountChannel", client)

    client.setInterval(client.emit("clearAFKChannel", client), client.config.TEMP_CHANNEL.Milliseconds_check)
}