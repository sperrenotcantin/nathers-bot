const { MessageEmbed, TextChannel } = require("discord.js");

module.exports = (client, member) => {

    client.emit("memberCountChannel", client)

    const channel = member.guild.channels.resolve(client.config.DEFAULTSETTING.welcome_channel)

    if(channel instanceof TextChannel) {
        const embed = new MessageEmbed()
            .setTitle('Une personne est arrivée.')
            .addField('Bienvenue ' + member.displayName, 'Nous sommes heureux de te voir ici !', true)
            .setDescription("Si tu as besoin d'aide n'hésite pas !")
            .setColor("#34c924")
            .setTimestamp()
        channel.send(embed)
    }
}