const { MessageEmbed, TextChannel } = require("discord.js");

module.exports = (client, member) => {
    client.emit("memberCountChannel", client)
    const channel = member.guild.channels.resolve(client.config.DEFAULTSETTING.welcome_channel)

    if(channel instanceof TextChannel) {
        const embed = new MessageEmbed()
            .setTitle('Une personne est partie.')
            .addField('Au revoir ' + member.displayName, 'Nous sommes déçu de te voir partir', true)
            .setDescription("Si tu as besoin d'aide n'hésite pas !")
            .setColor("BB0B0B")
            .setTimestamp()
        channel.send(embed)
    }
}