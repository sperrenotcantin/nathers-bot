module.exports = (client) => {

    const config = client.config.MEMBER_COUNT
    const guild = client.guilds.resolve(client.config.GUILD)
    const channel = client.channels.resolve(config.channel)
    const memberCount = guild.members.cache.array().length
    channel.edit({ name: config.name + guild.memberCount })
}