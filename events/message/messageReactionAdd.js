module.exports = async (client, messageReaction, user) => {
    if (messageReaction.message.channel.id === client.config.ROLES.channel) {
        return client.emit("reactionRoles", messageReaction, user)
    }

    //temporary channels
    client.database.get(client.models.MessageChannel, {message_id: messageReaction.message.id}).then(messageChannel => {
        if (!messageChannel.channel_id) return false;
        const guild = messageReaction.message.guild

        let channel = guild.channels.resolve(messageChannel.channel_id)
        channel.createOverwrite(user, {
            VIEW_CHANNEL: true
        })
    })
}