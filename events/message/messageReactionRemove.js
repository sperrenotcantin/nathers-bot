module.exports = (client, messageReaction, user) => {
    if (messageReaction.message.channel.id === client.config.ROLES.channel) {
        client.emit("reactionRoles", messageReaction, user)
    }

}