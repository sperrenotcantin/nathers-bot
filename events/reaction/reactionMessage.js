const {MessageEmbed} = require("discord.js")

module.exports = async (client) => {
    const config = client.config.ROLES
    const channel = client.channels.resolve(config.channel)

    await channel.bulkDelete(1)

    let embed = new MessageEmbed({
        title: "Reactions Roles",
        description: "Réagisser au message avec la réaction approprié au rôle souhaité.",
        color: client.config.COLOR
    })

    config.roles.forEach(element => {
        let roles = channel.guild.roles.resolve(element[0])
        embed.addField(roles.name, element[1], true)
    })

    let message = await channel.send(embed)

    config.roles.forEach(element => {
        message.react(element[1])
    })

}