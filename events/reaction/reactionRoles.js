module.exports = async (client, messageReaction, user) => {

    const config = client.config.ROLES
    let emoji = messageReaction.emoji
    let guild = messageReaction.message.channel.guild

    config.roles.forEach(element => {
        if (element[1] === emoji.toString() && !user.bot) {
            if (!guild.member(user).roles.cache.has(element[0])) {
                guild.member(user).roles.add(guild.roles.resolve(element[0]))
            } else {
                guild.member(user).roles.remove(guild.roles.resolve(element[0]))
            }
        }
    })
}