const {MessageEmbed} = require("discord.js")
module.exports = async (client) => {
    const twitterSettings = client.config.TWITTER
    const channel = client.channels.resolve(twitterSettings.channel_id)

    client.twitter.stream('statuses/filter', {follow: twitterSettings.user_id}, function (stream) {
        stream.on('data', function (tweet) {
            const showTweet = new MessageEmbed()
                .setColor('#00acee')
                .setAuthor(tweet.user.screen_name, tweet.user.profile_image_url_https, "https://twitter.com/" + tweet.user.screen_name)
                .setDescription(tweet.text)
                .setTimestamp(tweet.created_at)
                .setFooter("Nouveau tweet de " + tweet.user.screen_name, "https://cdn.cms-twdigitalassets.com/content/dam/about-twitter/en/brand-toolkit/brand-download-img-1.jpg.twimg.2560.jpg");
            channel.send(showTweet);
        });

        stream.on('error', function (error) {
            console.log(error);
        });
    });
    /**
    let last_ids = []

    setTimeout(() => {
        let old_follower = []
        let new_follower = []
        client.twitter.get("followers/ids", {cursor: -1, screen_name: twitterSettings.user_id}).then(element => {
            if (last_ids !== []) {
                last_ids.forEach(follower => {
                    if (!element.ids.includes(follower)) {
                        old_follower.push(follower)
                    }
                })
                element.ids.forEach(follower => {
                    if (!last_ids.includes(follower)) {
                        new_follower.push(follower)
                    }
                })
            }
            last_ids = element.ids
        }).catch(err => console.log(err))
        channel.send(new MessageEmbed({
            color: "#00acee",
            fields: [
                {
                    name: "Nouveaux Follower",
                    value: new_follower.length,
                    inline: true
                },
                {
                    name: "Perte de Follower",
                    value: old_follower.length,
                    inline: true
                }
            ]
        })
            .setAuthor("DelenclosNathan", "https://pbs.twimg.com/profile_images/1226509083458535424/WuDPZJx7_400x400.jpg", "https://twitter.com/DelenclosNathan")
            .setFooter("Twitter", "https://cdn.cms-twdigitalassets.com/content/dam/about-twitter/en/brand-toolkit/brand-download-img-1.jpg.twimg.2560.jpg"));
    }, 30 * 1000)*/
}