const Twitter = require("twitter");
const {Client, Collection} = require("discord.js");
const {readdirSync} = require("fs")

const client = new Client();
require("./util/function")(client)
client.config = require("./config")
client.models = require("./Models/index")
client.mongoose = require("./util/mongoose");
["commands", 'cooldowns', 'welcome_channel'].forEach(x => client[x] = new Collection());

const loadCommands = (dir = "./commands/") => {
    readdirSync(dir).forEach(dirs => {
        const commands = readdirSync(`${dir}/${dirs}/`).filter(files => files.endsWith(".js"));

        for (const file of commands) {
            const getFileName = require(`${dir}/${dirs}/${file}`);
            client.commands.set(getFileName.help.name, getFileName)
            console.log(`Command chargée: ${getFileName.help.name}`)
        }
    })
}

const loadEvents = (dir = "./events/") => {
    readdirSync(dir).forEach(dirs => {
        const events = readdirSync(`${dir}/${dirs}/`).filter(files => files.endsWith(".js"));

        for (const event of events) {
            const evt = require(`${dir}/${dirs}/${event}`);
            const evtName = event.split(".")[0];
            client.on(evtName, evt.bind(null, client))
            console.log(`Evenement chargé: ${evtName}`)
        }
    })
}

loadCommands();
loadEvents();

client.twitter = new Twitter({
    consumer_key: client.config.TWITTER.consumer_key,
    consumer_secret: client.config.TWITTER.consumer_secret,
    access_token_key: client.config.TWITTER.access_token_key,
    access_token_secret: client.config.TWITTER.access_token_secret,
});

client.mongoose.init()

client.login(client.config.TOKEN)