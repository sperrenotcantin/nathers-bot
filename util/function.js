const mongoose = require("mongoose");
const { Guild, Poll } = require("../Models/index")

module.exports = async client => {
    client.createGuild = async guild => {
        const merged = Object.assign({_id: mongoose.Types.ObjectId()}, guild);
        const createGuild = await new Guild(merged)
        createGuild.save().then(g => console.log(`Nouveau server -> ${g.guildName}`));
    }

    client.getGuild = async guild => {
        const data = await Guild.findOne({guildID: guild.id});
        if (data) return data;
        return client.config.DEFAULTSETTING;
    }

    client.updateGuild = async (guild, settings) => {
        let data = await client.getGuild(guild);

        if (typeof data !== "object") data = {};
        for (const key in settings) {
            if (data[key] !== settings[key]) data[key] = settings[key];
        }
        return data.updateOne(settings)
    }

    client.createPoll = async poll => {
        const merged = Object.assign({_id: mongoose.Types.ObjectId()}, poll);
        const createPoll = await new Poll(merged)
        createPoll.save().then(() => console.log(`Nouveau Poll`));
    }

    client.getPoll = async poll => {
        const data = await Poll.findOne({_id: poll._id});
        if (data) return data;
        return poll;
    }

    client.updatePoll = async (poll, settings) => {
        let data = await client.getPoll(poll);

        if (typeof data !== "object") data = {};
        for (const key in settings) {
            if (data[key] !== settings[key]) data[key] = settings[key];
        }
        return data.updateOne(settings)
    }
    client.database = {
        create: async (Model, data) => {
            const merged = Object.assign({_id: mongoose.Types.ObjectId()}, data);
            const createPoll = await new Model(merged)
            createPoll.save().then(() => console.log(`New element in the database`));
            return createPoll
        },

        get: async (Model, data) => {
            const element = await Model.findOne(data);
            if (element) return element;
            return undefined;
        },
        update: async (Model, data, settings) => {
            let element = await client.database.get(Model, data);

            if (typeof element !== "object") element = {};
            for (const key in settings) {
                if (element[key] !== settings[key]) element[key] = settings[key];
            }
            return element.updateOne(settings)
        }
    }
}